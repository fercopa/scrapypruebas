# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs

from datetime import date
from recolistor.spiders.compania import *


def format_allianz_fecha(fecha):
    date = (str(fecha).split(" ")[0].split("-"))
    date.reverse()
    return "/".join(date)


class QbeSpider(CompaniaSpider):
    allowed_host = "https://www.oficinasvirtuales.qbe.com.ar/"
    name = 'qbe'
    login_url = "http://www.oficinasvirtuales.qbe.com.ar/AccesoCAM30/Login/Login.asp"
    start_urls = [login_url]

    def download(self, response):
        for month, year in self.iter_months():
            self.get_files_operaciones(month, year)
        self.zip()

    def login(self):
        self.get(self.login_url)
        self.find(By.NAME, "usuario", wait=True).send_keys(self.user)
        time.sleep(3)
        self.find(
            By.NAME, "clave", wait=True).send_keys(self.password)
        self.click(self.find(
            By.XPATH,
            "//img[@src='/AccesoCAM30/images/btn_continuar_az.gif']"))
        time.sleep(3)

        fields = self.find_all(By.XPATH, "//cuerpo/*")
        self.identificador = "hsbc4040"
        i = 0
        for field in fields:
            if field.tag_name == 'input':
                field.send_keys(self.identificador[i])
            i += 1
        self.click(
            self.find(
                By.XPATH,
                "//img[@src='/AccesoCAM30/images/btn_continuar_az.gif']"))
        time.sleep(3)

    def is_logged(self):
        text_ok = "Seleccione Número de Productor"
        return self.find(By.XPATH, self.xpath_contains(text_ok)) is not None

    def get_files_operaciones(self, month, year):
        # informes
        self.click(self.find(By.ID, "Stm0p2i3eMTD"))
        filas = [fila.find_elements_by_xpath(".//td") for fila in  self.find_all(By.XPATH, "//tr")]
        filas_sin_headers = [fila for fila in filas if len(fila)==6][2:]
        filas_en_rango_fecha = [fila for fila in filas_sin_headers if date( int(fila[0].text.split('/')[2]), int(fila[0].text.split('/')[1]), 1) > self.date_from.date()]
        lupa = filas_en_rango_fecha[-1][5].find_element_by_xpath(".//img[@src='/Oficina_Virtual/HTML/Images/boton_buscar.gif']")
        self.click(lupa)
        time.sleep(10)
