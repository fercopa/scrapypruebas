# -*- coding: utf-8 -*-
"""
Scrapy: Mapfre
Actualizado al: 10/2018

Este script se encarga de descargar los archivos de dicha compañía.
Login
https://web2.mapfre.com.ar/mapfrenet/login.aspx

"""
import time
from compania import CompanyScrapy, OPERACION, COBRANZA
from selenium.webdriver.support.ui import Select

URL_BASE = "https://web2.mapfre.com.ar/mapfrenet/"
URL_COBRANZA = URL_BASE + 'sitio/cobranzas/cob_mensual/selector.aspx'
URL_OPERACION = URL_BASE + 'sitio/emision/libros/EmisionLibros.aspx'
# Type of file
FILE_CSV = 2  # The value of field "Operacion"


class MapfreSpider(CompanyScrapy):
    def __init__(self, *args, **kwargs):
        """
        Mapfre
        """
        super(MapfreSpider, self).__init__(*args, **kwargs)
        self.login_url = URL_BASE + "login.aspx"
        self.last_name = 'SGOIFO'

    def login(self):
        """
        Log in to the company page.
        """
        self.driver.get(self.login_url)
        time.sleep(1)
        # user
        user_attr_name = "ctl00$body$usuario"
        field_name = self.driver.find_element_by_name(user_attr_name)
        field_name.send_keys(self.username)
        # password
        password_attr_name = "ctl00$body$clave"
        field_clave = self.driver.find_element_by_name(password_attr_name)
        field_clave.send_keys(self.password)
        # login
        btn_send_name = "ctl00$body$btSend"
        btn_send = self.driver.find_element_by_name(btn_send_name)
        btn_send.click()
        # self.frame("MM")

    def is_logged(self):
        text_ok = "tituloMenu"
        return self.driver.find_element_by_id(text_ok) is not None

    def get_codes_of_productor(self):
        """
        Returns a list of codes.
        """
        ret = []
        select_name = 'ctl00$body$SelectorPAS'
        elem = self.driver.find_element_by_name(select_name)
        select = Select(elem)
        cant = len(select.options)
        for i in range(cant):
            prod = select.options[i].text.lower()
            value = self.get_value_from_string(prod)
            last_name = self.last_name.lower()
            if '/' not in prod and last_name in prod:
                ret.append(value)
        return ret

    def get_value_from_string(self, s):
        """
        Returns the value for select's option.

        Params:
            s -- String like "<LAST_NAME> + <NAMES> + (<value>)"

        For example: if the string is 'PEREZ JUAN PABLO (1234)' so, returns
        1234.
        """
        ret = s.split()[-1] if s else ''
        ret = ret.strip('(').strip(')')  # Remove the parenthesis
        return ret

    def there_are_movs(self):
        """
        Returns True if "Cantidad de filas en el archivo" is greater than 1.
        Oterwise False
        """
        # '/html/body/form/div[5]/table/tbody/tr/td/'
        # 'table/tbody/tr[1]/td[2]'
        xpath = "/html/body/form/div[5]/table/tbody/tr/td/" + \
                "table/tbody/tr[1]/td[2]"
        try:
            elem = self.driver.find_element_by_xpath(xpath)
            ret = int(elem.text)
        except Exception as e:
            ret = 0
            print(e)
        return True if ret > 1 else False

    def download(self, book=OPERACION):
        # "//tr[@class='HeaderStyle']/th[@scope='col']"):
        url = URL_COBRANZA if book else URL_OPERACION
        self.driver.get(url)
        if book:
            self.download_cobranza()
        else:
            self.download_operacion()

    def download_operacion(self):
        """
        Download the Operations files.
        """
        self.set_operacion()
        for code in self.get_codes_of_productor():
            self.set_productor(code)
            for fecha in self.generate_date():
                d, m, y = fecha.split('/')
                self.set_month(m)
                self.set_year(y)
                self.click_continuar()
                self.click_download()

    def set_operacion(self):
        """
        Select the 'Operacion' field.
        """
        name = 'ctl00$body$DropDownListOperacion'
        select = Select(self.driver.find_element_by_name(name))
        select.select_by_value(str(FILE_CSV))
        time.sleep(1)

    def set_productor(self, value):
        """
        Select the 'Productor' field.
        """
        name = "ctl00$body$SelectorPAS"
        select = Select(self.driver.find_element_by_name(name))
        # value = self.get_codes_of_productor()[0]
        select.select_by_value(value)
        time.sleep(1)

    def set_month(self, value):
        """
        Select the 'Mes' field given a value.

        Params:
            value -- A string like 'mm' (month)
        """
        value = value.strip('0')
        name = "ctl00$body$cboMes"
        select = Select(self.driver.find_element_by_name(name))
        select.select_by_value(value)
        time.sleep(1)

    def set_year(self, value):
        """
        Select the 'Anio' field given a value.

        Params:
            value -- A string like 'YYYY'
        """
        name = 'ctl00$body$cboAnio'
        select = Select(self.driver.find_element_by_name(name))
        select.select_by_value(value)
        time.sleep(1)

    def click_continuar(self):
        # Button Continuar
        btnId = "ctl00_body_BtSubmit"
        btn = self.driver.find_element_by_id(btnId)
        btn.click()
        time.sleep(2)

    def click_download(self):
        elem = self.driver.find_element_by_id("ctl00_body_btDescarga")
        if self.there_are_movs():
            elem.click()
        time.sleep(1)
        self.driver.back()

    def download_cobranza(self):
        pass


if __name__ == '__main__':
    try:
        m = MapfreSpider('Mapfre', username='7557', password='gra7557',
                         date_from='01/01/2016', date_to='29/02/2016')
        m.set_navigator()
        m.login()
        time.sleep(2)
        m.download()
        m.download(COBRANZA)
    except Exception as e:
        print(e)
    finally:
        m.quit()
