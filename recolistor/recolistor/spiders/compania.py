import os
from datetime import datetime
from dateutil.relativedelta import relativedelta
from selenium import webdriver
from seleniumrequests import Firefox

DATE_FORMAT = "%d/%m/%Y"

# Type of book
OPERACION = 0
COBRANZA = 1


class Navigator():
    # Type of files to download
    FILE_TYPES = [
            "application/octet-stream",
            "text/plain",  # .txt
            "text/csv",  # .csv
            "application/csv",  # .csv
            "application/vnd.ms-excel",  # .xls
            "application/xml",  # .xml
            "application/zip",  # .zip
            ]

    def __init__(self, download_to='/tmp/'):
        """
        Configure a navigator with a custom profile and set up a folder for
        downloading files.
        Params:
            download_to -- Path to folder. Default /tmp/
        """
        self._profile = webdriver.FirefoxProfile()

        # Configuration for Downloading files
        # Folder to downloading 0, 1 or 2
        # 0: Descktop, 1: Download, # 2: Most recent download
        self._profile.set_preference("browser.download.folderList", 2)
        self._profile.set_preference("browser.download.dir", download_to)

        # Show when starting, default true
        showWhenStarting = "browser.download.manager.showWhenStarting"
        self._profile.set_preference(showWhenStarting, False)

        # Download directly without asking
        types_files = ','.join(self.FILE_TYPES)
        self._profile.set_preference(
                "browser.helperApps.neverAsk.saveToDisk", types_files)
        self._profile.set_preference(
                "browser.helperApps.alwaysAsk.force", False)

    def get_driver(self):
        """
        Returns a webdriver of Firefox.
        """
        log_path = os.path.join(os.path.abspath(''), 'webdriver.log')
        return Firefox(firefox_profile=self._profile, log_path=log_path)


class CompanyScrapy:
    """
    Extract information of a company web page.

    Params:
        username -- The username for login
        password -- The password for login
        date_from -- Date initial for extract
        date_to -- Date final for extract
        company_name -- Name of current company
    """
    def __init__(self, company_name, username='', password='', date_from=None,
                 date_to=None, *args, **kwargs):
        self.username = username
        self.password = password
        self.date_from = date_from
        self.date_to = date_to
        self.company_name = company_name
        self.url_login = ''
        # Rut dir
        self.tmp_folder = '/tmp/ScrapyCompany/'
        os.makedirs(self.tmp_folder, exist_ok=True)
        self.driver = None

    def set_navigator(self):
        """
        Set up a driver for Firefox and create a temporal dir of the company
        """
        company_dir = os.path.join(self.tmp_folder, self.company_name)
        os.makedirs(company_dir, exist_ok=True)
        self.driver = Navigator(download_to=company_dir).get_driver()

    def quit(self):
        """
        Close the browser
        """
        self.driver.quit()

    def generate_date(self):
        """
        Returns a date like '01/11/2015' from date_from to date_to.
        If date_from or date_to is '', returns ''
        """
        try:
            date_from = datetime.strptime(self.date_from, DATE_FORMAT)
            date_to = datetime.strptime(self.date_to, DATE_FORMAT)
            a_month = relativedelta(months=1)
            while date_from < date_to:
                yield date_from.strftime(DATE_FORMAT)
                date_from = date_from + a_month
        except ValueError as e:
            return ''
