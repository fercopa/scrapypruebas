# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs
import datetime
import calendar
import os

from recolistor.spiders.compania import *


class NorteSpider(CompaniaSpider):
    name = 'norte'
    allowed_host = "http://sistemas.elnorte.com.ar/"
    login_url = "http://sistemas.elnorte.com.ar/portal/"
    start_urls = [login_url]
    downloads = 0

    def download(self, response):
        self.get_polizas()
        self.get_cobranzas()
        count_files = len(os.listdir(os.getcwd()))
        if count_files < self.downloads:
            self.logger.error("Count Files" + str(count_files))
            self.logger.error("self.downloads" + str(self.downloads))
            time.sleep(10)
            self.logger.error("Waiting for downloads")
        self.zip()

    def login(self):
        self.get(self.login_url)
        # user
        self.find(By.ID, "usuario").send_keys(self.user)
        # password
        self.find(By.ID, "password").send_keys(self.password)
        # confirmar
        self.click(self.find(By.ID, "btnIngresar"))
        self.main_frame()

    def is_logged(self):
        return self.find(By.CLASS_NAME, "cerrarSesion") is not None

    def get_polizas(self):
        # herramientas
        elem = (self.find_all(By.XPATH, "//p[@class='menu_nivel_inicial']"))[8]
        elem.click()
        self.find(By.XPATH, "//ul[@id='bloque_submenu56']/li[@value='43']/a").click()

        # libro operaciones
        self.frame("")
        cant_meses = len(list(self.iter_months()))

        i = 0
        for month,year in self.iter_months():
            try:
                self.frame("contenido")
            except:
                pass

            # fecha s
            if i == 0:
                fecha = datetime.date(year, month, int(self.date_from.strftime("%d/%m/%Y").split("/")[0]))
            else:
                fecha = datetime.date(year, month, 1)
            time.sleep(3)
            self.find(By.NAME, "f_desde").clear()
            self.set_input(By.NAME, "f_desde", fecha.strftime("%d/%m/%Y"))
            if i == cant_meses - 1:
                fecha = datetime.date(year, month, int(self.date_to.strftime("%d/%m/%Y").split("/")[0]))
            else:
                fecha = datetime.date(year, month, calendar.monthrange(year, month)[1])
            self.find(By.NAME, "f_hasta").clear()
            self.set_input(By.NAME, "f_hasta", fecha.strftime("%d/%m/%Y"))
            # exportar en excel y xml
            self.click(self.find(By.ID, "expor"))
            self.downloads += 1
            time.sleep(2)
            i += 1

    def get_cobranzas(self):
        # herramientas
        self.main_frame()
        self.find(By.XPATH, "//ul[@id='bloque_submenu56']/li[@value='44']/a").click()


        # libro cobranzas
        self.frame("")
        cant_meses = len(list(self.iter_months()))

        i = 0
        for month,year in self.iter_months():
            try:
                self.frame("contenido")
            except:
                pass

            # fechas
            if i == 0:
                fecha = datetime.date(year, month, int(self.date_from.strftime("%d/%m/%Y").split("/")[0]))
            else:
                fecha = datetime.date(year, month, 1)
            time.sleep(3)
            self.find(By.XPATH, "//input[@id='f_desde']").clear()
            self.set_input(By.NAME, "f_desde", fecha.strftime("%d/%m/%Y"))
            if i == cant_meses:
                fecha = datetime.date(year, month, int(self.date_to.strftime("%d/%m/%Y").split(" ")[0]))
            else:
                fecha = datetime.date(year, month, calendar.monthrange(year, month)[1])
            self.find(By.NAME, "f_hasta").clear()
            self.set_input(By.NAME, "f_hasta", fecha.strftime("%d/%m/%Y"))
            # exportar en excel y xml
            self.click(self.find(By.ID, "expor"))
            self.downloads += 1

            time.sleep(10)
