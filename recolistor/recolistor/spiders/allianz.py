# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs


from recolistor.spiders.compania import *


def format_allianz_fecha(fecha):
    date = (str(fecha).split(" ")[0].split("-"))
    date.reverse()
    return "/".join(date)


class AllianzSpider(CompaniaSpider):
    allowed_host = "https://mb.allianznet.com.ar/"
    name = 'allianz'
    login_url = "https://mb.allianznet.com.ar/allianz/extranet/inicio.conexion"
    start_urls = [login_url]

    def download(self, response):
        self.get_polizas_and_cobranzas()
        self.zip()

    def login(self):
        self.get(self.login_url)
        self.find(By.NAME, "uname", wait=True).send_keys(self.user)
        password = self.find(By.NAME, "upass").send_keys(self.password)
        img = self.find(By.XPATH, "//img[@src='/img/bot_aceptar.gif']").click()

    def is_logged(self):
        return self.find(By.NAME, "DATOS") is not None

    def get_polizas_and_cobranzas(self):
        # informes
        time.sleep(5)
        self.get("https://mb.allianznet.com.ar/allianz/extranet/REG_PRODUCTORES_OPERACIONES.consultas_operaciones_par")
        time.sleep(5)
        self.find(By.NAME, "P_FECHA_DESDE").send_keys(Keys.CONTROL + "a")
        self.find(By.NAME, "P_FECHA_DESDE").send_keys(format_allianz_fecha(self.date_from))
        self.find(By.NAME, "P_FECHA_HASTA").send_keys(Keys.CONTROL + "a")
        self.find(By.NAME, "P_FECHA_HASTA").send_keys(format_allianz_fecha(self.date_to))
        elem = self.find(By.NAME, "procesar", wait=True)
        self.click(elem)
        time.sleep(5)
        self.switch_to_window(self.driver.window_handles[1])
        self.wait(By.XPATH,  "//img[@src='/iconos/txt.gif']", time=10)
        self.click(self.find_all(By.XPATH, "//img[@src='/iconos/txt.gif']")[1])
        time.sleep(60)

        self.get("https://mb.allianznet.com.ar/allianz/extranet/REG_PRODUCTORES_COBRANZAS.consultas_operaciones_par")
        time.sleep(5)
        self.find(By.NAME, "P_FECHA_DESDE").send_keys(Keys.CONTROL + "a")
        self.find(By.NAME, "P_FECHA_DESDE").send_keys(format_allianz_fecha(self.date_from))
        self.find(By.NAME, "P_FECHA_HASTA").send_keys(Keys.CONTROL + "a")
        self.find(By.NAME, "P_FECHA_HASTA").send_keys(format_allianz_fecha(self.date_to))
        elem = self.find(By.NAME, "procesar", wait=True)
        self.click(elem)
        time.sleep(5)
        self.switch_to_window(self.driver.window_handles[1])
        self.wait(By.XPATH,  "//img[@src='/iconos/txt.gif']", time=10)
        self.click(self.find_all(By.XPATH, "//img[@src='/iconos/txt.gif']")[1])
        time.sleep(60)
