# -*- coding: utf-8 -*-
"""
Scrapy: Mapfre
Actualizado al: 10/2018

Script para descargar de la compania San Cristobal.
Los url tienen en comun un link llamado BASE.
base_url = https://pas.sancristobal.com.ar/PortalSCNet/

Los pasos que sigue el script son:
- Va al portal de login
base_url + Productores/HomeProductores.aspx
- Una vez logueado va la portal de descargas
base_url + Productores/Descargas/Descargas.aspx?download=libros
base_ids = 'ctl00_ContentPlaceHolder1_ctl01'
En el html viene un elemento
<select id = base_ids + 'OrganizadorProductor1_CboProductores'>
si el productor tiene muchos codigos. Si no, no viene este elemento.
Por lo tanto si encuentra este elemento, descarga para todos los codigos.
Si no lo encuentra, solo descarga de un productor.

Logout
base_url + productores/login/Loguin.aspx?command=1

Algunos datos que pueden interesar del HTML
Productor
<option selected="selected" value="3|5043|2022561163">
    03-5043 FULGENZI, PABLO DANIEL
</option>
<option value="3|5211|2022561163">03-5211 FULGENZI, PABLO DANIEL</option>
<option value="3|5245|2022561163">03-5245 FULGENZI, PABLO DANIEL</option>
<option value="3|5257|2022561163">03-5257 FULGENZI, PABLO DANIEL</option>

Fechas
<option value="201004">04/2010</option>
<option value="201005">05/2010</option>

Link descarga
<a id="ctl00_ContentPlaceHolder1_ctl01_HypLibros"
href=base_url + "temp/xls/CobranzaPortal5043201501.csv" target="_blank">
Descargar excel</a>

Nota: La mayoría de las veces, en el campo para seleccionar el productor
(si es que tiene varios códigos) aparece solo un productor con diferentes
códigos. Por lo tanto se programó asumiendo que es el mismo productor.
En otras compañías suele venir productores mezclados, por ejemplo:
<option selected value=algún valor> 5043 FULGENZI, PABLO DANIEL </option>
<option value=algún valor>5211 PEREZ, Fabricio</option>
<option value=algún valor>5245 DOMINGUEZ, JUAN</option>
<option value=algún valor>5257 FULGENZI, PABLO DANIEL</option>
Por lo tanto si llegara de esta forma, abría que modificar el algoritmo para
que considere esta forma. Para ello nos servirá el CUIT (sin el último número,
ya que en el HTML viene así) el cual podrá tomar como parametro.
"""

# import os
import time
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

# from recolistor.spiders.compania import *
from compania import CompanyScrapy, OPERACION, COBRANZA

URL_BASE = "https://pas.sancristobal.com.ar/PortalSCNet/Productores"
BASE_IDS = 'ctl00_ContentPlaceHolder1_ctl01'

# Usuario con varios productores
USER_TEST = "ORGANIZA5002"
PASS_TEST = "salta34"

# Usuario con un productor
# USER_TEST = "PEDREIRA3904"
# PASS_TEST = "B5DE3A"


class SanCristobal(CompanyScrapy):

    def __init__(self, *args, **kwargs):
        super(SanCristobal, self).__init__(*args, **kwargs)
        self.url_login = '/'.join((URL_BASE, 'HomeProductores.aspx', ))
        self.url_download = '/'.join((URL_BASE, 'Descargas',
                                     'Descargas.aspx?download=libros'))
        # self.cuit = cuit

    def download(self):
        """
        Download the operetions and cobranzas files.
        """
        self.driver.get(self.url_download)
        time.sleep(1)
        elem = self.get_elementID_of_productor()
        if 'LblProductor' in elem:  # A single code
            self.download_book(book=COBRANZA)
            self.download_book(book=OPERACION)
        else:  # Many codes
            self.download_book_with_productores(elem, book=COBRANZA)
            self.download_book_with_productores(elem, book=OPERACION)

    def get_elementID_of_productor(self):
        """
        Returns the id of <select> if the productor has many codes or the id
        of element, where is the info of productor.
        """
        productor = 'OrganizadorProductor1_LblProductor'
        productores = 'OrganizadorProductor1_CboProductores'
        try:  # If the user has many codes
            elem_id = '_'.join((BASE_IDS, productores))
            self.driver.find_element_by_id(elem_id)
        except NoSuchElementException as e:  # The user has only a code
            # En el logs describir e
            elem_id = '_'.join((BASE_IDS, productor))
        return elem_id

    def login(self):
        """
        Log in to the company page.
        """
        self.driver.get(self.url_login)
        # user
        userid = "AccPanelLogin_content_TxtUsuario"
        user = self.driver.find_element_by_id(userid)
        user.send_keys(USER_TEST)
        # password
        passwordid = "AccPanelLogin_content_TxtContraseña"
        password = self.driver.find_element_by_id(passwordid)
        password.send_keys(PASS_TEST)
        # confirmar
        btnid = "AccPanelLogin_content_BtnIngresar"
        elem = self.driver.find_element_by_id(btnid)
        elem.click()
        time.sleep(1)

    def logout(self):
        url_logout = URL_BASE + '/login/Loguin.aspx?command=1'
        self.driver.get(url_logout)
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.alert_is_present())
        alert = self.driver.switch_to.alert
        alert.accept()

    def get_select_month_id(self, book):
        """
        Returns the id of select of field mes according to value of book.

        Params:
            book -- Type of book, 0 for Operaciones (default) and
            1 for Cobranzas.
        """
        # IDs of element <select>:
        # cob: 'ctl00_ContentPlaceHolder1_ctl01_DdlFechas'
        # ope: 'ctl00_ContentPlaceHolder1_ctl01_DdlFechasOperaciones'
        cobranza = 'ctl00_ContentPlaceHolder1_ctl01_DdlFechas'
        operacion = 'ctl00_ContentPlaceHolder1_ctl01_DdlFechasOperaciones'
        return cobranza if book else operacion

    def get_download_link_id(self, book):
        """
        Returns the id of element <a> according to value of book.

        Params:
            book -- Type of book, 0 for Operaciones (default) and
            1 for Cobranzas.
        """
        # ID <a>:
        # cob: 'ctl00_ContentPlaceHolder1_ctl01_HypLibros'
        # ope: 'ctl00_ContentPlaceHolder1_ctl01_HlkOperaciones'
        link_base = 'ctl00_ContentPlaceHolder1_ctl01_'
        cobranza_link = link_base + 'HypLibros'
        operacion_link = link_base + 'HlkOperaciones'
        return cobranza_link if book else operacion_link

    def download_book(self, book=OPERACION):
        """
        Download operations or cobranzas.

        Params:
            book -- Type of book, 0 for Operaciones (default) and
            1 for Cobranzas.
        """
        # Build the ID of <select> and the ID of <a>
        select_mesesId = self.get_select_month_id(book)
        linkId = self.get_download_link_id(book)
        for fecha in self.generate_date():
            time.sleep(1)
            meses = self.driver.find_element_by_id(select_mesesId)
            select_meses = Select(meses)
            value = self.format_date(fecha)  # Convert to YYYYmm
            select_meses.select_by_value(value)
            element = WebDriverWait(self.driver, 60)
            xpath = "//a[@id='%s'][contains(@href, '%s')]" % (linkId, value)
            loc = (By.XPATH, xpath)
            element.until(EC.presence_of_element_located(loc))
            link = self.driver.find_element_by_id(linkId)
            link.click()

    def download_book_with_productores(self, select_id, book=OPERACION):
        """
        Download books with many codes of productor.

        Params:
            select_id -- WebElement select where is the codes.
            book -- Type of book, 0 for Operaciones (default) and
            1 for Cobranzas.
        """
        elem = self.driver.find_element_by_id(select_id)
        select_productores = Select(elem)
        amount_codes = len(select_productores.options)
        for code in range(amount_codes):
            elem = self.driver.find_element_by_id(select_id)
            select_productores = Select(elem)
            select_productores.select_by_index(code)
            self.download_book(book=book)

    def format_date(self, fecha):
        """
        Returns a value of date like YYYYmm.

        Params:
            fecha -- String like dd/mm/YYYY
        """
        try:
            d, m, y = fecha.split('/')
            ret = y + m
        except Exception as e:
            ret = ''
        return ret


if __name__ == '__main__':
    try:
        sc = SanCristobal("914", "San cristobal",
                          date_from='01/11/2018',
                          date_to='10/12/2018')
        sc.set_navigator()
        sc.login()
        sc.download()
        sc.logout()
        time.sleep(5)
    except Exception as e:
        print(e)
    finally:
        sc.quit()
