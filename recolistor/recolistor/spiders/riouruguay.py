"""
Scrapy: Río Uruguay
Actualizado al: 10/2018

Este script se encarga de descargar los archivos de dicha compañía.

Los pasos que sigue son los siguientes:
- Inicia sesión en el sistema
- Crea la url (create_link) de solicitud de descarga de archivo con los
siguientes datos:
    * Matrìcula
    * Fecha desde dd/mm/aaaa
    * Fecha hasta
    * Formato (default xls)
    Para poder trabajar de esta forma (sin ir navegando por páginas
intermedias) se usa la librería selenium-request se hace un GET y nos devuelve
un response en donde nos viene el archivo.
- Guarda el archivo. Este último tiene una forma particular de guardar y
necesita que la variable local DOWNLOAD_ROOT_PATH esté definida.
"""
import os
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from compania import CompanyScrapy, DATE_FORMAT
import time


DOWNLOAD_ROOT_PATH = 'Descargas'
URL_LOGIN = "https://extranet.rus.com.ar/servlets/index.jsp"


class RioUruguaySpider(CompanyScrapy):

    def __init__(self, matricula, *args, **kwargs):
        super(RioUruguaySpider, self).__init__(*args, **kwargs)
        self.matricula = matricula
        self.url_login = URL_LOGIN

    def login(self):
        """
        Log in to the company page.
        """
        if self.driver:
            print("AQUIIIIII>>>>>> ", self.username, self.password)
            try:
                self.driver.get(self.url_login)
                # Wait the java form (alert) for a maximum of 10 seconds
                wait = WebDriverWait(self.driver, 10)
                wait.until(EC.alert_is_present())
                # Select to alert box and sends username and password
                alert = self.driver.switch_to.alert
                # alert.send_keys(self.username)
                time.sleep(1)
                alert.send_keys(self.username + Keys.TAB + self.password)
                time.sleep(2)
                alert.accept()
                time.sleep(2)
                self.driver = alert.driver
            except Exception as e:
                pass

    def download(self):
        """
        Download the files of the Rio Uruguay company.
        """
        if self.checks_requeriments():
            self.get_operations_and_cobranzas()

    def checks_requeriments(self):
        """
        Returns True if the fields are corrects.

        The fields necessary are: matricula, company_name, date_from and
        date_to
        Raise ValueError if matricula is empty or is a matricula invalid. Or
        if date_from or date_to is a date string invalid.
        """
        try:
            int(self.matricula)
            company_name_is_empty = True if not self.company_name else False
            datetime.strptime(self.date_from, DATE_FORMAT)
            datetime.strptime(self.date_to, DATE_FORMAT)
            ret = not company_name_is_empty
        except ValueError as e:
            # TODO: Ver lo de logging
            print(e)
            ret = False
        return ret

    def get_operations_and_cobranzas(self):
        """
        Gets and saves the files zip that contains operations and cobranzas.
        """
        for d in self.generate_date():
            date_to = self.get_the_last_day(d)
            if d and date_to:
                print("Solicitando desde %s al %s" % (d, date_to))
                link = self.create_link(d, date_to)
                response = self.driver.request('GET', link)
                self.save_file_from_response(response)

    def create_link(self, date_from, date_to):
        """
        Returns a link formated with date_from and date_to.

        Params:
            date_from -- Init date like string 'dd/mm/YYYY'
            date_to -- Final date like string 'dd/mm/YYYY'
        """
        base_url = "https://extranet.rus.com.ar/servlets/"
        items = ['riouruguay', 'servlets', 'productores', 'registro',
                 'RegistroProductores?']
        conditions = ['desde=%s' % date_from,
                      'hasta=%s' % date_to,
                      'idMatricula=%s' % self.matricula,
                      'formato=xls', 'ordinal=1']
        link = base_url + '.'.join(items) + '&'.join(conditions)
        return link

    def save_file_from_response(self, response):
        """
        Save the file received.

        Params:
            response -- Instance of Webdriver.request
        """
        cd = response.headers.get('Content-Disposition', '')
        if cd and 'filename' in cd:
            # cd shoud be like 'attachment filename=<Name of file>'
            filename = cd.split('=')[-1]
            if filename:
                print('Descargado: %s' % filename)
                pathfile = os.path.join(DOWNLOAD_ROOT_PATH, filename)
                with open(pathfile, 'wb') as f:
                    for chunk in response.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)

    def get_the_last_day(self, fecha):
        """
        Returns the date of the last day of month.

        Params:
            fecha -- Date with format dd/mm/YYYY
        Returns:
            str -- The date of the last day of month as a string with format
            dd/mm/YYYY.
            For example: If fecha is 01/02/2016, so returns 29/02/2016
        """
        ret = ''
        try:
            current_date = datetime.strptime(fecha, DATE_FORMAT)
            month = relativedelta(months=1)
            day = timedelta(days=1)
            new_date = current_date + month - day
            ret = new_date.strftime(DATE_FORMAT)
        except ValueError as e:
            pass
        return ret


if __name__ == '__main__':
    ru = RioUruguaySpider('46372', 'rus', date_from='01/05/2018',
                          date_to='31/10/2018', username='productor/2212',
                          password='2s09sp')
    ru.set_navigator()
    ru.login()
    ru.download()
    ru.quit()
